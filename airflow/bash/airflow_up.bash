#!/bin/bash
airflow scheduler -D && echo "====== scheduler up! ======"
airflow webserver -D -p 8080 -H 0.0.0.0 && echo "====== webserver up! ======"