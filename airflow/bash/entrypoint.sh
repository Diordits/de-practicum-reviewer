#!/bin/bash
export AIRFLOW_HOME=/home/airflow
export AIRFLOW_VERSION=2.2.4
PYTHON_VERSION="$(python3 --version | cut -d " " -f 2 | cut -d "." -f 1-2)"
CONSTRAINT_URL="https://raw.githubusercontent.com/apache/airflow/constraints-${AIRFLOW_VERSION}/constraints-${PYTHON_VERSION}.txt"
pip install "apache-airflow==${AIRFLOW_VERSION}" --constraint "${CONSTRAINT_URL}"
airflow db init
airflow users create \
  --email airflow@airflow.ru \
  --firstname airflow \
  --lastname airflow \
  --password airflow \
  --role Admin \
  --username airflow