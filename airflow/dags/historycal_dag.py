import os
import sys

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago

sys.path.insert(0, '/home')
sys.path.insert(0, os.environ['HOME_APP'])

from app.gnrl import historical

default_args = {
    'owner': 'Diordits Pavel',
    'start_date': days_ago(0),
    'depends_on_past': False
}

dag = DAG('historical_run', default_args=default_args, schedule_interval='@once', catchup=False)

t0 = PythonOperator(
    dag=dag,
    task_id='go',
    python_callable=historical)

t0
