FROM python:3.9

ENV AIRFLOW_HOME /home/airflow
COPY airflow/ $AIRFLOW_HOME/

ENV HOME_APP /home/app
COPY app/ $HOME_APP/
VOLUME $HOME_APP/configs

RUN /bin/bash $AIRFLOW_HOME/bash/entrypoint.sh && \
    apt-get update -qq && \
    apt-get install -y \
        iproute2 \
        postgresql-client \
        net-tools \
        nmap \
        htop \
        mc \
        nano \
        tmux && \
    pip install -r $HOME_APP/requirements.txt

CMD airflow db init && \
    airflow users create \
        --email airflow@airflow.ru \
        --firstname airflow \
        --lastname airflow \
        --password airflow \
        --role Admin \
        --username airflow && \
        /bin/bash $AIRFLOW_HOME/bash/airflow_up.bash

EXPOSE 8080
