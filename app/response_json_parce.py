from datetime import datetime as dt
from pprint import pprint


class Historical_load:
    def __init__(self, json):
        self.json = json
        self.from_ = self.json['base']
        self.items = self.json['rates'].items()
        self.data_to_insert = self.map()

    def historical_json_transform(self, i):
        to_ = i[1].keys()
        date_ = i[0]
        value_ = i[1].values()

        return self.from_, *to_, date_, *value_

    def map(self):
        return list(map(self.historical_json_transform, self.items))


class Intime_load:
    def __init__(self, json):
        self.json = json
        pprint(json)
        self.query = self.json['query']
        self.data_to_insert = self.intime_json_transform()

    def intime_json_transform(self):
        datetime_ = dt.now()
        from_ = self.query['from']
        to_ = self.query['to']
        rate = self.json['result']
        return from_, to_, datetime_.__str__(), rate


hjson = {'base': 'USD',
         'end_date': '2020-04-03',
         'start_date': '2020-03-01',
         'rates': {
             '2020-03-01': {'RUB': 66.319996},
             '2020-03-02': {'RUB': 66.817754},
             '2020-03-03': {'RUB': 66.39878},
             '2020-03-04': {'RUB': 66.175297},
             '2020-03-05': {'RUB': 66.265464},
             '2020-03-06': {'RUB': 67.559337},
             '2020-03-07': {'RUB': 67.559337},
             '2020-04-01': {'RUB': 78.512832},
             '2020-04-02': {'RUB': 78.847544},
             '2020-04-03': {'RUB': 77.129898}
            }
         }

ijson = {'date': '2022-04-03',
         'historical': False,
         'info': {'rate': 85.692428},
         'query': {'amount': 1, 'from': 'USD', 'to': 'RUB'},
         'result': 85.692428,
         'success': True}

if __name__ == "__main__":
    x = Historical_load(hjson).data_to_insert
    print(x)
