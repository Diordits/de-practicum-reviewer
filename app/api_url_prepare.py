from urllib.parse import urlencode
from app.tools import config, PATHS


class UrlPrepare:
    """
    Для формирования сторки запрсоа к API нужно указать файл конфигруацц (config_file), определяющий сценарий работы:
            1. Загрузка исторических данных - 'configs/endpoints/historical.cfg'
            2. Итерационная загрузка с конфигруруемым интервалом - 'configs/endpoints/convert.cfg'
    """

    def __init__(self, config_file):
        self.conf_file = config_file
        self._options_()
        self.url = self.path + self.query

    def _options_(self):
        self.conf = config(self.conf_file)

        self.path = self.conf['endpoint']['path']
        self.query = urlencode({**self.conf["date interval"], **self.conf["convert"]})


if __name__ == "__main__":
    x = UrlPrepare(config_file=PATHS.intime_load)
    print(x.url)
