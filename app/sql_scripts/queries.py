class CreateTable:
    historical = """
            CREATE SCHEMA IF NOT EXISTS practicum;
            CREATE TABLE IF NOT EXISTS practicum.exchange_h (
                id SERIAL, 
                exch_from varchar(3),
                exch_to varchar(3),
                rates real,
                load_dtime timestamp)
            """.strip()

    intime = """
            CREATE SCHEMA IF NOT EXISTS practicum;
            CREATE TABLE IF NOT EXISTS practicum.exchange_i (
                id SERIAL, 
                exch_from varchar(3),
                exch_to varchar(3),
                rates real,
                load_dtime timestamp)
            """.strip()


class InsertInto:
    historical = "insert into practicum.exchange_h " \
                 "(exch_from, exch_to, load_dtime, rates) values ('{}', '{}', '{}', '{}')"
    intime = "insert into practicum.exchange_i (exch_from, exch_to, load_dtime, rates) values ('{}', '{}', '{}', '{}')"


if __name__ == "__main__":
    x = getattr(CreateTable, "history")
    print(x)
