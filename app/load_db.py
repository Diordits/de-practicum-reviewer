from app.sql_scripts.queries import CreateTable, InsertInto
from app.tools import config, take_engine


def execute(f):
    def wrap(*args, **kwargs):
        engine = take_engine()
        with engine.connect() as conn:
            f(conn, *args, **kwargs)

    return wrap


@execute
def create_table(conn, tmplt, *args):
    """tmplt - сцентарий [intime, historical]"""
    query = getattr(CreateTable, tmplt)
    conn.execute(query)


@execute
def insert_into(conn, tmplt, format_args):
    print("->>", format_args)

    if tmplt == "intime":
        query = getattr(InsertInto, tmplt).format(*format_args)
        print('-->', query)
        conn.execute(query)

    elif tmplt == "historical":
        for date in format_args:
            query = getattr(InsertInto, tmplt).format(*date)
            print('-->', query)
            conn.execute(query)


if __name__ == "__main__":
    cfg = config('configs/db/postgres.cfg')
    engine = take_engine(cfg)
    insert_into(tmplt='intime', format_args=('USD', 'RUB', '2022-04-03 16:40:56.132543', 85.692428))
