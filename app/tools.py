import configparser
from pprint import pprint

import sqlalchemy as sa
import requests
from os.path import join
import os, sys

home = os.environ["HOME_APP"]


class PATHS:
    postgres = join(home, 'configs/db/postgres.cfg')
    airflow = join(home, 'configs/airflow')
    airflow_dags = join(home, 'configs/airflow/dags')
    intime_load = join(home, 'configs/endpoints/convert.cfg')
    historical_load = join(home, 'configs/endpoints/historical.cfg')


def get_request(req):
    response = requests.get(req)
    if response.ok:
        data = response.json()
        return data


def config(conf_file):
    cfg = configparser.ConfigParser()
    cfg.read(conf_file)
    return cfg


def take_engine(path=PATHS.postgres):
    cfg = config(path)

    conn_params = dict(cfg['connection'])
    conn_string = 'postgresql+psycopg2://{user}:{password}@{host}:{port}/{db}'.format(**conn_params)
    return sa.engine.create_engine(conn_string)


if __name__ == "__main__":
    pprint(PATHS.__dict__)
    pass
