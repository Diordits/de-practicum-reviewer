from app.api_url_prepare import UrlPrepare
from app.response_json_parce import Historical_load, Intime_load
from app.tools import get_request, PATHS
from app.load_db import insert_into, create_table


def intime():
    req_url = UrlPrepare(PATHS.intime_load).url
    print(req_url)
    data = get_request(req_url)
    data_tl = Intime_load(data)

    create_table('intime')
    insert_into('intime', data_tl.data_to_insert)


def historical():
    req_url = UrlPrepare(PATHS.historical_load).url
    data = get_request(req_url)
    data_tl = Historical_load(data)

    create_table('historical')
    insert_into('historical', data_tl.data_to_insert)


if __name__ == "__main__":
    intime()
    # intime()
    pass
